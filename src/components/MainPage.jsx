import React, { Component } from 'react';
import InfoCard from "./InfoCard";


class MainPage extends Component {

  state = {
    currentItem: null,
  };

  constructor(props) {
    super(props);

    this._renderPeopleNames = this._renderPeopleNames.bind(this);
    this._renderClickedUser = this._renderClickedUser.bind(this);
  }

  _renderPeopleNames = (users) => {
    return users.people.map((item, i) => {
      return (
        <div key={i} onClick={() => this._renderClickedUser(item)}
             style={{cursor: 'pointer', padding: '8px'}}>{i + 1 + '. ' + item.name}</div>
      );
    });
  };

  _renderClickedUser = (item) => {
    this.setState({currentItem: item});
  };

  render() {
    const {users} = this.props;

    if (users.people === undefined || users.people.length === 0) {
      return <div>loading ....</div>;
    }

    return (
      <div>
        <h3>List of People</h3>
        <h5>Please click user name to view full details</h5>
        <hr/>
        {this._renderPeopleNames(users)}
        <br/>
        <div>
          <InfoCard user={this.state.currentItem}/>
        </div>
      </div>
    )
  }
}

export default MainPage;