import React, { Component } from 'react';


class InfoCard extends Component {
  render() {
    const {user} = this.props;

    if (user === null || user === undefined) {
      return null;
    }

    return (
      <div>
        <hr/>
        <div>
          <h3>Person Details Section</h3>
        </div>
        <table>
          <tbody>
          <tr>
            <td>Name :</td>
            <td>{user.name}</td>
          </tr>
          <tr>
            <td>Eye Color :</td>
            <td>{user.eye_color}</td>
          </tr>
          <tr>
            <td>Gender :</td>
            <td>{user.gender}</td>
          </tr>
          <tr>
            <td>Hair Color :</td>
            <td>{user.hair_color}</td>
          </tr>
          <tr>
            <td>Height :</td>
            <td>{user.height}</td>
          </tr>
          <tr>
            <td>Mass :</td>
            <td>{user.mass}</td>
          </tr>
          </tbody>

        </table>
      </div>
    )
  }
}

export default InfoCard;