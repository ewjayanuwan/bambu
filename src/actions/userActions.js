import axios from 'axios'

export const getPeopleData = () => {
  return (dispatch) => {
    axios.get('https://swapi.co/api/people')
      .then((response) => {
        dispatch({type: 'FETCH_PEOPLE_DATA', payload: response.data.results})
      });
  }
}
