import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as userActions from '../actions/userActions'
import MainPage from '../components/MainPage'

class HomeContainer extends Component {
  componentWillMount () {
    this.props.getPeopleData()
  }

  render() {
    return (
      <MainPage {...this.props}/>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPeopleData: () => {
      dispatch(userActions.getPeopleData())
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
