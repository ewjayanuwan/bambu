let initialState = {};

const users = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_PEOPLE_DATA':
      return {
        ...state,
        people: action.payload
      };
    default:
      return state
  }
};

export default users;
