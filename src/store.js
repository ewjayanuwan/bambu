import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk';
import reducers from './reducers/index'
import logger from 'redux-logger'

const store = createStore(
  reducers,
  applyMiddleware(thunkMiddleware, logger)
);

export default store;